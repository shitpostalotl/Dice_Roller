import random

roll = input("Input roll: ")
result = 0
try:
	ammount = range(0, int(roll[:roll.find("d")]))
	sides = int(roll[roll.find("d")+1:])
except ValueError:
	ammount = [1]
	sides = int(roll)

for i in ammount: result = result + int(random.randint(1, sides))
print(result)